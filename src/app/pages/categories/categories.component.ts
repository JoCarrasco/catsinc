import { Component, OnInit } from '@angular/core';
import { ICategory } from 'src/app/models/category.model';
import { CatDispatcherService } from 'src/app/services/cat-dispatcher/cat-dispatcher.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  isLoading = false;
  categories: ICategory[] = [];
  categoryImgs: any[] = [];
  constructor(private catDispatcherService: CatDispatcherService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories(): void {
    this.isLoading = true;
    this.catDispatcherService.getAllCategories().then((categories) => {
      this.categories = categories;
      this.isLoading = false;
      this.getCategoriesImgs(this.categories);
    });
  }

  getCategoriesImgs(categories: ICategory[]): void {
    const categoryIds = categories.map(category => category.id);
    this.catDispatcherService.getCategoryImgs(categoryIds).then((imgs) => {
      this.categoryImgs = imgs.map(category => {
        return {
          height: category[0].height,
          url: this.sanitizer.bypassSecurityTrustStyle(`url(${category[0].url})`)
        }
      });
    });
  }

  resizeGridItem(item) {
    const grid = document.getElementsByClassName('grid')[0];
    const rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'), 10);
    const rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'), 10);
    const rowSpan = Math.ceil((item.querySelector('.content').getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));
    item.style.gridRowEnd = 'span ' + rowSpan;
  }

  resizeAllGridItems() {
    const allItems = document.getElementsByClassName('item');
    for (let x = 0; x < allItems.length; x++) {
      this.resizeGridItem(allItems[x]);
    }
  }

  onResize(e) {
    this.resizeAllGridItems();
  }

}
