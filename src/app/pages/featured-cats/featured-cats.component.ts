import { Component, OnInit } from '@angular/core';
import { CatDispatcherService } from 'src/app/services/cat-dispatcher/cat-dispatcher.service';
import { ICat } from 'src/app/models';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FiltersComponent } from 'src/app/components/filters/filters.component';

@Component({
  selector: 'app-featured-cats',
  templateUrl: './featured-cats.component.html',
  styleUrls: ['./featured-cats.component.scss']
})
export class FeaturedCatsComponent implements OnInit {
  breedsDisplayed = 15;
  isDisplay = 'featured';
  isLoading = false;
  displayBreeds: ICat[] = [];
  breakpoint: number;
  featuredBreeds: ICat[] = [];
  filterBreeds: ICat[] = [];
  featuredImgUrls: string[] = [];
  filterModal: MatDialogRef<FiltersComponent, any>;

  constructor(private catDispatcherService: CatDispatcherService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getFeaturedBreeds();
  }

  getFeaturedBreeds(): void {
    this.isLoading = true;
    this.setBreakpointByWidth(window.innerWidth);
    this.getBreeds(this.breedsDisplayed);
  }

  onResize(event): void {
    const viewportWidth: number = event.target.innerWidth;
    this.setBreakpointByWidth(viewportWidth);
  }

  setBreakpointByWidth(viewportWidth: number): void {
    if (viewportWidth <= 537) {
      this.breakpoint = 1;
    } else if (viewportWidth <= 828) {
      this.breakpoint = 2;
    } else if (viewportWidth <= 1020) {
      this.breakpoint = 3;
    } else {
      this.breakpoint = 4;
    }
  }

  openFilters(): void {
    this.filterModal = this.dialog.open(FiltersComponent, {
      width: '320px'
    });
    this.filterModal.afterClosed().toPromise().then((filterObj: { breedName: string; origin: string }) => {
      this.filterCats(filterObj.breedName, filterObj.origin);
    });
  }

  getBreeds(quantity: number): void {
    this.displayBreeds = [];
    this.featuredBreeds = [];
    this.isLoading = true;
    this.catDispatcherService.getFeaturedCatBreeds(quantity).then(cats => {
      this.featuredBreeds = cats;
      this.displayBreeds = this.featuredBreeds;
      this.isLoading = false;
      this.getCatsImg(cats);
    }).catch((e) => this.isLoading = false);
  }

  filterCats(breedName: string, origin: string): void {
    this.filterBreeds = this.filterCombiation('name', breedName, 'origin', origin);

    if (!breedName.length && !origin.length) {
      this.displayBreeds = this.featuredBreeds;
    } else {
      this.isDisplay = 'filter';
      this.displayBreeds = this.filterBreeds;
    }
    this.getCatsImg(this.displayBreeds);
  }


  private filterCombiation(prop: string, value: string, prop2?: string, value2?: string): ICat[] {
    return this.featuredBreeds.filter((cat) => {
      if (prop) {
        if (prop2 && prop2.length) {
          if (cat[prop].toLowerCase().includes(value.toLowerCase()) && cat[prop2].toLowerCase().includes(value2.toLowerCase())) {
            return cat;
          }
        } else {
          if (cat[prop].toLowerCase().includes(cat[prop].toLowerCase())) {
            return cat;
          }
        }
      } else {
        return;
      }
    });
  }

  getCatsImg(cats: ICat[]): void {
    const breedsId: string[] = cats.map((cat) => cat.id);
    this.catDispatcherService.getCatBreedsImgs(breedsId).then((imgs) => {
      this.featuredImgUrls = imgs.map(img => img[0].url);
    });
  }
}
