import { async, ComponentFixture, TestBed, inject, getTestBed } from '@angular/core/testing';

import { FeaturedCatsComponent } from './featured-cats.component';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';
import { CatCardComponent } from 'src/app/components/cat-card/cat-card.component';
import { CatInfoComponent } from 'src/app/components/cat-info/cat-info.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CatDispatcherService } from 'src/app/services/cat-dispatcher/cat-dispatcher.service';
import { ICat } from 'src/app/models';
import { mockBreed, mockBreedImage, mockFilterModalData } from 'src/app/static';
import { DebugElement, Component } from '@angular/core';
import { of, } from 'rxjs';
import { MatDialog } from '@angular/material';
import { By } from '@angular/platform-browser';
import { FiltersComponent } from 'src/app/components/filters/filters.component';
import { MatDialogMock, DispatcherMock } from 'src/app/static/testing.classes';


@Component({
  template: ''
})
class NoopComponent { }

const TEST_DIRECTIVES = [
  FiltersComponent,
  NoopComponent
];

describe('FeaturedCatsComponent', () => {
  let component: FeaturedCatsComponent;
  let fixture: ComponentFixture<FeaturedCatsComponent>;
  let dialog: MatDialog;
  let debugElement: DebugElement;
  let dialogSpy: jasmine.Spy;
  let service: CatDispatcherService;
  const dialogRefSpyObj = jasmine.createSpyObj({ afterClosed: of({}), close: null });
  dialogRefSpyObj.componentInstance = { body: '' };
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [FormsModule, MaterialModule, HttpClientTestingModule, BrowserAnimationsModule],
      providers: [CatDispatcherService, {
        provide: MatDialog, useClass: MatDialogMock
      }],
      declarations: [FeaturedCatsComponent, LoadingComponent, CatCardComponent, CatInfoComponent],
    }).compileComponents();
  });

  beforeEach(async () => {
    dialogSpy = spyOn(TestBed.get(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
    dialog = TestBed.get(MatDialog);
    fixture = TestBed.createComponent(FeaturedCatsComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    service = TestBed.get(CatDispatcherService);
    component.ngOnInit();
    await fixture.whenStable();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#getFeaturedBreeds should render featured breeds', () => {
    const breakpointSpy = spyOn(component, 'setBreakpointByWidth');
    const getBreedsSpy = spyOn(component, 'getBreeds');
    component.getFeaturedBreeds();
    expect(component.isLoading).toBe(true);
    expect(breakpointSpy).toHaveBeenCalled();
    expect(getBreedsSpy).toHaveBeenCalled();
  });

  it('#openFilters() should open a modal', () => {
    component.openFilters();
    expect(dialogSpy).toHaveBeenCalled();
  });

  it('Modal should return data when closed', () => {
    component.openFilters();
    expect(dialogSpy).toHaveBeenCalled();
    component.filterModal.close();
    expect(dialogRefSpyObj.afterClosed).toHaveBeenCalled();
  });
});
