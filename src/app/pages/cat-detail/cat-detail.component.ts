import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ICat } from 'src/app/models';
import { ActivatedRoute } from '@angular/router';
import { CatDispatcherService } from 'src/app/services/cat-dispatcher/cat-dispatcher.service';

@Component({
  selector: 'app-cat-detail',
  templateUrl: './cat-detail.component.html',
  styleUrls: ['./cat-detail.component.scss']
})
export class CatDetailComponent implements OnInit {
  @ViewChild('catImg', { static: true }) catImg: ElementRef;
  @ViewChild('catAvatar', { static: true }) catAvatar: ElementRef;
  catId: string;
  cat: ICat;
  catImgURL: string;
  constructor(private route: ActivatedRoute, private catDispatcherService: CatDispatcherService) { }

  ngOnInit() {
    this.catId = this.route.snapshot.paramMap.get('id');
    this.getCat();
  }

  getCat() {
    this.catDispatcherService.getCatBreedImgById(this.catId).then((objArr) => {
      const img = this.catImg.nativeElement as HTMLDivElement;
      const avatar = this.catAvatar.nativeElement as HTMLDivElement;
      if (objArr.length) {
        const imgURL = `url(${objArr[0].url})`;
        this.cat = objArr[0].breeds[0];
        console.log(this.cat);
        avatar.style.backgroundImage = imgURL;
        img.style.backgroundImage = imgURL;
      }
    });
  }
}
