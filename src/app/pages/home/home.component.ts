import { Component, OnInit } from '@angular/core';
import { AppRoutes } from 'src/app/static';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  enterApp(): void {
    this.router.navigate([AppRoutes.Dashboard]);
  }
}
