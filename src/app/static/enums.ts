/**
 * All CRUD actions available for http requests.
 */
export enum CRUDAction {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete'
}
