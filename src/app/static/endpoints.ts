export const baseURL = 'https://api.thecatapi.com/v1';

export const breedsURL = `${baseURL}/breeds`;
export const categoriesURL = `${baseURL}/categories`;
export const searchImg = `${baseURL}/images/search`;
