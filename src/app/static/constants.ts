import { ICat } from '../models';
import { ICatImgObject } from '../services/cat-dispatcher/cat-dispatcher.service';
import { ICategory, ICategoryResponse } from '../models/category.model';

export const mockBreed: ICat = {
  adaptability: 5,
  affection_level: 5,
  alt_names: '',
  cfa_url: 'http://cfa.org/Breeds/BreedsAB/Abyssinian.aspx',
  child_friendly: 3,
  country_code: 'EG',
  country_codes: 'EG',
  description: 'The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.',
  dog_friendly: 4,
  energy_level: 5,
  experimental: 0,
  grooming: 1,
  hairless: 0,
  health_issues: 2,
  hypoallergenic: 0,
  id: 'abys',
  indoor: 0,
  intelligence: 5,
  lap: 1,
  life_span: '14 - 15',
  name: 'Abyssinian',
  natural: 1,
  origin: 'Egypt',
  rare: 0,
  rex: 0,
  shedding_level: 2,
  short_legs: 0,
  social_needs: 5,
  stranger_friendly: 5,
  suppressed_tail: 0,
  temperament: 'Active, Energetic, Independent, Intelligent, Gentle',
  vcahospitals_url: 'https://vcahospitals.com/know-your-pet/cat-breeds/abyssinian',
  vetstreet_url: 'http://www.vetstreet.com/cats/abyssinian',
  vocalisation: 1,
  weight: {
    imperial: '7  -  10',
    metric: '3 - 5'
  },
  wikipedia_url: 'https://en.wikipedia.org/wiki/Abyssinian_(cat)'
};

export const mockBreedImage: ICatImgObject = {
  breeds: [
    {
      weight: {
        imperial: '7  -  10',
        metric: '3 - 5'
      },
      id: 'abys',
      name: 'Abyssinian',
      cfa_url: 'http://cfa.org/Breeds/BreedsAB/Abyssinian.aspx',
      vetstreet_url: 'http://www.vetstreet.com/cats/abyssinian',
      vcahospitals_url: 'https://vcahospitals.com/know-your-pet/cat-breeds/abyssinian',
      temperament: 'Active, Energetic, Independent, Intelligent, Gentle',
      origin: 'Egypt',
      country_codes: 'EG',
      country_code: 'EG',
      description: 'The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.',
      life_span: '14 - 15',
      indoor: 0,
      lap: 1,
      alt_names: '',
      adaptability: 5,
      affection_level: 5,
      child_friendly: 3,
      dog_friendly: 4,
      energy_level: 5,
      grooming: 1,
      health_issues: 2,
      intelligence: 5,
      shedding_level: 2,
      social_needs: 5,
      stranger_friendly: 5,
      vocalisation: 1,
      experimental: 0,
      hairless: 0,
      natural: 1,
      rare: 0,
      rex: 0,
      suppressed_tail: 0,
      short_legs: 0,
      wikipedia_url: 'https://en.wikipedia.org/wiki/Abyssinian_(cat)',
      hypoallergenic: 0
    }
  ],
  id: '0XYvRd7oD',
  url: 'https://cdn2.thecatapi.com/images/0XYvRd7oD.jpg',
  width: 1204,
  height: 1445
};


export const mockCategory: ICategory = {
  id: 5,
  name: 'boxes'
};

export const mockFilterModalData: { name: string; origin: string } = {
  name: 'abys', origin: 'u'
}

export const mockCategoryImage: ICategoryResponse = {
  breeds: [],
  categories: [
    {
      id: 5,
      name: 'boxes'
    }
  ],
  id: 'h8',
  url: 'https://cdn2.thecatapi.com/images/h8.jpg',
  width: 540,
  height: 720
};
