import { of } from 'rxjs';
import { mockBreedImage, mockBreed } from './constants';

export class MatDialogMock {
  open() {
    return {
      afterClosed: () => of({ action: true })
    };
  }
}

export class DispatcherMock {
  getCatBreedsImgs() {
    return new Promise((r) => r([[mockBreedImage]]));
  }
  getFeaturedCatBreeds() {
    return new Promise((r) => r([mockBreed]));
  }
}
