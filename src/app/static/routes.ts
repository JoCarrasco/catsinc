enum AppRoutes {
  Home = 'home',
  Dashboard = 'dashboard',
  CatDetail = 'cat-detail',
  FeaturedCats = 'featured-cats',
  Categories = 'categories'
}

export { AppRoutes };
