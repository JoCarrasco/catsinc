export interface ICategory {
  id: number;
  name: string;
}

export interface ICategoryResponse {
  breeds: any[];
  categories: ICategory[];
  id: string;
  url: string;
  width: number;
  height: number;
}
