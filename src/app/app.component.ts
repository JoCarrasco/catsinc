import { Component } from '@angular/core';
import { ApiService } from './services/api/api.service';
import { ICat } from './models';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
