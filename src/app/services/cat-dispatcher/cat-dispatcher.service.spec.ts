import { TestBed } from '@angular/core/testing';

import { CatDispatcherService } from './cat-dispatcher.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { mockBreed, mockBreedImage, mockCategory, mockCategoryImage } from 'src/app/static/constants';
import { breedsURL, searchImg, categoriesURL } from 'src/app/static';

describe('CatDispatcherService', () => {
  let service: CatDispatcherService;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CatDispatcherService
      ]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(CatDispatcherService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getFeaturedCatBreeds should return a proper value', () => {
    service.getFeaturedCatBreeds(1).then((cats) => {
      expect(cats.length).toBe(1);
      expect(cats[0].name).toEqual(mockBreed.name);
    });

    const req = httpTestingController.expectOne(`${breedsURL}?limit=1`);
    expect(req.request.method).toBe('GET');
    req.flush([mockBreed]);
  });

  it('#getCatBreedsImgs should return a proper value', () => {
    service.getCatBreedImgById(mockBreed.id).then((cat) => {
      expect(cat.length).toBe(1);
      expect(cat[0].breeds.length).toBe(1);
      expect(cat[0].breeds[0].name).toBe(mockBreed.name);
    });

    const req = httpTestingController.expectOne(`${searchImg}?breed_ids=${mockBreed.id}`);
    expect(req.request.method).toBe('GET');
    req.flush([mockBreedImage]);
  });

  it('#getAllCategories should return a proper value', () => {
    service.getAllCategories().then((categories) => {
      expect(categories.length).toBeGreaterThan(0);
      expect(categories[0]).toEqual(mockCategory);
    });

    const req = httpTestingController.expectOne(`${categoriesURL}`);
    expect(req.request.method).toBe('GET');
    req.flush([mockCategory]);
  });

  it('#getCategoryImgs should return a proper value', () => {
    service.getCategoryImgs([mockCategory.id]).then((categoryImgs) => {
      expect(categoryImgs.length).toBe(1);
      expect(categoryImgs[0].length).toBe(1);
      expect(categoryImgs[0][0].categories.length).toBe(1);
      expect(categoryImgs[0][0].categories[0].id).toEqual(mockCategory.id);
      expect(categoryImgs[0][0].url).toEqual(mockCategoryImage.url);
    });

    const req = httpTestingController.expectOne(`${searchImg}?category_ids=${mockCategory.id}`);
    expect(req.request.method).toBe('GET');
    req.flush([mockCategoryImage]);
  });
});
