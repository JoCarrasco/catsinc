import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { ICat } from 'src/app/models';
import { Observable, forkJoin } from 'rxjs';
import { ICategory, ICategoryResponse } from 'src/app/models/category.model';
import { breedsURL, searchImg, categoriesURL } from 'src/app/static';

@Injectable({
  providedIn: 'root'
})
export class CatDispatcherService {
  constructor(private apiService: ApiService) { }

  getFeaturedCatBreeds(limit: number): Promise<ICat[]> {
    return this.apiService.get<ICat[]>(breedsURL, { limit }).toPromise();
  }

  getCatBreedImgById(id: string): Promise<ICatImgObject[]> {
    return this.apiService.get<ICatImgObject[]>(`${searchImg}?breed_ids=${id}`).toPromise();
  }

  getCatBreedsImgs(idArr: string[]): Promise<Array<ICatImgObject[]>> {
    const requestArr: Observable<any>[] = idArr.map(id => this.apiService.get<ICatImgObject[]>(`${searchImg}?breed_ids=${id}`));
    return forkJoin(requestArr).toPromise();
  }

  getAllCategories(): Promise<ICategory[]> {
    return this.apiService.get<ICategory[]>(categoriesURL).toPromise();
  }

  getCategoryImgs(idArr: number[]): Promise<Array<ICategoryResponse[]>> {
    const requestArr = idArr
      .map(id => this.apiService.get<ICategoryResponse[]>(`${searchImg}?category_ids=${id}`));
    return forkJoin(requestArr).toPromise();
  }
}

export interface ICatImgObject {
  breeds: ICat[];
  id: string;
  url: string;
  width: number;
  height: number;
}
