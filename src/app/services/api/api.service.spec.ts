import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { ICat } from 'src/app/models';
import { mockBreed} from 'src/app/static/constants';
import { breedsURL } from 'src/app/static';

describe('Api Service', () => {
  let injector: TestBed;
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ApiService
      ]
    });

    injector = getTestBed();
    service = injector.get(ApiService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    const serviceInstance: ApiService = TestBed.get(ApiService);
    expect(serviceInstance).toBeTruthy();
  });

  it('get() should return data', () => {
    service.get<ICat[]>(breedsURL, { limit: 1 }).toPromise().then(cats => {
      expect(cats.length).toBe(1);
      expect(cats[0].name).toEqual('Abyssinian');
    });

    const req = httpMock.expectOne(`${breedsURL}?limit=1`);
    expect(req.request.method).toBe('GET');
    req.flush([mockBreed]);
  });
});

