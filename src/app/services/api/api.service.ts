import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CRUDAction } from 'src/app/static/enums';
import { Params } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ApiService {

  constructor(public http: HttpClient) {

  }

  /**
   * Retrieve a resource from a target URL.
   * @param url - The target URL.
   * @param auth - true if request requires Authorization Header, false if it doesn't.
   */
  get<T>(url: string, params?: Params): Observable<T> {
    return this.genericRequest<T>(CRUDAction.GET, url, undefined, params);
  }

  /**
   * Create a resource using a target URL.
   * @param url - The target URL.
   * @param body - The body of the request.
   * @param auth - true: Sends request with an "Authorization" header, false: no "Authorization" header is provided
   */
  post<T>(url: string, body: any, auth: boolean = true): Observable<T> {
    return this.genericRequest<T>(CRUDAction.POST, url, body);
  }

  /**
   * Updates a resource using a target URL.
   * @param url - The target URL.
   * @param body - The body of the request.
   * @param auth - true: Sends request with an "Authorization" header, false: no "Authorization" header is provided
   */
  put<T>(url: string, body: any, auth: boolean = true): Observable<T> {
    return this.genericRequest<T>(CRUDAction.PUT, url, body);
  }

  /**
   * Initializes a generic request that supports the basic CRUD actions
   * @param crudAction - The action to be taken.
   * @param url - The target URL.
   * @param data - The object that we want to send for POST/PUT Crud Actions.
   */
  private genericRequest<T>(
    crudAction: CRUDAction,
    url: string,
    data?: any,
    params?: Params): Observable<T> {
    const apiKey = '9e5769dc-9e84-4e62-85de-01573d36e2b0';
    let request: Observable<any>;
    let apiHeaders = new HttpHeaders();
    apiHeaders = apiHeaders.append('x-api-key', apiKey);

    if (crudAction === CRUDAction.POST || crudAction === CRUDAction.PUT) {
      if (data === undefined) { return; }
      request = this.http[crudAction](url, data, { headers: apiHeaders, params });
    } else if (crudAction === CRUDAction.GET || crudAction === CRUDAction.DELETE) {
      request = this.http[crudAction](url, { headers: apiHeaders, params });
    }

    if (request) {
      return request;
    }
  }
}
