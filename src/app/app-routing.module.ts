import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutes } from './static';
import { CatDetailComponent } from './pages/cat-detail/cat-detail.component';
import { HomeComponent } from './pages/home/home.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FeaturedCatsComponent } from './pages/featured-cats/featured-cats.component';
import { CategoriesComponent } from './pages/categories/categories.component';


const routes: Routes = [
  { path: '', redirectTo: AppRoutes.Home, pathMatch: 'full' },
  { path: AppRoutes.Home, component: HomeComponent },
  {
    path: AppRoutes.Dashboard, component: DashboardComponent, children: [
      { path: '', redirectTo: AppRoutes.FeaturedCats, pathMatch: 'full' },
      { path: AppRoutes.FeaturedCats, component: FeaturedCatsComponent },
      { path: AppRoutes.Categories, component: CategoriesComponent },
      { path: `${AppRoutes.CatDetail}/:id`, component: CatDetailComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
