import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { FiltersComponent } from './filters.component';
import { MaterialModule } from 'src/app/material/material.module';
import { MatDialogRef, MatDialog } from '@angular/material';
import { OverlayContainer } from '@angular/cdk/overlay';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('FiltersComponent', () => {
  let component: FiltersComponent;
  let fixture: ComponentFixture<FiltersComponent>;
  const mockDialogRef = {
    close: jasmine.createSpy('close')
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        MaterialModule
      ],
      providers: [{
        provide: MatDialogRef,
        useValue: mockDialogRef
      }],
      declarations: [FiltersComponent]
    })


      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#submit should close the dialog and send data', () => {
    component.submit('aegean', 'united states');
    expect(mockDialogRef.close).toHaveBeenCalled();
  });
});
