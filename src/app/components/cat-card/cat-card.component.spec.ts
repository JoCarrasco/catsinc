import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CatCardComponent } from './cat-card.component';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/static';
import { MaterialModule } from 'src/app/material/material.module';
import { CatInfoComponent } from '../cat-info/cat-info.component';

describe('CatCardComponent', () => {
  const testID = '2';
  let component: CatCardComponent;
  let fixture: ComponentFixture<CatCardComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CatCardComponent, CatInfoComponent],
      imports: [
        MaterialModule,
        RouterTestingModule.withRoutes([])
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatCardComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    component.cat = {
      id: testID,
      name: 'test',
      description: 'testdesc'
    };
    expect(component).toBeTruthy();
  });

  it('should navigate to detail cat page', () => {
    const componentInstance = fixture.componentInstance;
    const navigateSpy = spyOn(router, 'navigate');

    componentInstance.cat = {
      id: testID,
      name: 'test',
      description: 'testdesc'
    };

    componentInstance.goToCatDetail();
    expect(navigateSpy).toHaveBeenCalledWith([AppRoutes.Dashboard + '/' + AppRoutes.CatDetail, testID]);
  });
});
