import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/static';
import { ICat } from 'src/app/models';

@Component({
  selector: 'app-cat-card',
  templateUrl: './cat-card.component.html',
  styleUrls: ['./cat-card.component.scss'],
})
export class CatCardComponent implements OnInit, OnChanges {
  private url = '../../../assets/img/cat_img_placeholder.gif';

  @Input() cat: ICat;
  @Input() catImg: string;

  get name(): string {
    return this.url;
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.url = changes.catImg.currentValue;
    console.log(changes);
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

  }

  goToCatDetail(): void {
    this.router.navigate([`${AppRoutes.Dashboard}/${AppRoutes.CatDetail}`, this.cat.id]);
  }

  getShortDescription(str: string): string {
    return str.length <= 150 ? str : `${str.slice(0, 193)}...`;
  }
}
