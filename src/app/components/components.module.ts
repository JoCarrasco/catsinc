import { CatCardComponent } from './cat-card/cat-card.component';
import { AppRoutingModule } from '../app-routing.module';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material/material.module';
import { NavbarComponent } from './navbar/navbar.component';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading/loading.component';
import { CatInfoComponent } from './cat-info/cat-info.component';
import { FiltersComponent } from './filters/filters.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  entryComponents: [FiltersComponent],
  declarations: [CatCardComponent, NavbarComponent, LoadingComponent, CatInfoComponent, FiltersComponent],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    MaterialModule
  ],
  exports: [
    CatCardComponent,
    NavbarComponent,
    CatInfoComponent,
    LoadingComponent,
    FiltersComponent
  ]
})
export class ComponentsModule { }
