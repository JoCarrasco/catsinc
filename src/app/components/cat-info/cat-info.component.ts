import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cat-info',
  templateUrl: './cat-info.component.html',
  styleUrls: ['./cat-info.component.scss']
})
export class CatInfoComponent implements OnInit {
  @Input() lifeSpan: string;
  @Input() origin: string;
  @Input() childFriendly?: number;
  @Input() dogFriendly?: number;
  @Input() isVertical?: boolean;

  constructor() { }

  ngOnInit() {
  }

}
