import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/static';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
  }

  goTo(route: string) {
    if (route === 'featured') {
      this.router.navigate([`${AppRoutes.Dashboard}`]);
    } else if (route === 'categories') {
      this.router.navigate([`${AppRoutes.Dashboard}/${AppRoutes.Categories}`]);
    }
  }
}
